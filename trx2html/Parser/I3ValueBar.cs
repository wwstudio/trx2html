﻿using System;
namespace trx2html.Parser
{
    public interface I3ValueBar
    {
        double PercentIgnored { get; }
        double PercentKO { get; }
        double PercentOK { get; }
    }
}
