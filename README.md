# README #

This project is forked from https://github.com/ridomin/RidoTasks
We forked this project because we want to bring trx2htm task into our CI process. But to meet our situation, we need to tweak it first.

### What is new in this fork ###

* Migrate solution from vs2010 to vs2013
* trx2htm MSBuild task now support OutputDirectory of generated htm files
* Added more log which will dump to msbuild console when executing the task